![Current Version](https://img.shields.io/badge/version-1.0.0-brightgreen.svg?style=flat "1.0.0")


# Business Rule DSL 

Business Rule DSL (RuleDsl) is designed to describe business-specific rules for recommendations (e.g.  Products based on a Product). It declaratively specify the rules to filter the recommendation resultset in the context of a Recommendation System. 

## Introduction 

In this section, we describe the syntax and semantics of the RuleDSL. 

### Syntax

A simplified formal definition of the RuleDSL language with EBNF is outlined below. 

```
DslProgram  = { Ruledef } EOF; // a program can have multiple rules
Ruledef     = rule RuleName “:“ RuleDefBody;
RuleDefBody = WhenExpr SelectExpr; 
WhenExpr    = "when" Expr
FromExpr    = ("select" | "show") DataSet “with” Expr;
Expr        = ID Op Literal;
Factor      = ALL | Literal | "(" ~> Expr <~ ")" | ID
Op          =   “eq” 
               | “is” 
               | “not eq” 
               | “is not”
               | “in”
               | “not in”
               | “and”
               | “or” 
Literal  = String | List | Integer
List     = “[“ {String, “,”}* “]” 
ID       = [a-zA-Z][a-zA-Z0-9]*
DataSet  = “Product”;   
RuleName = String | ID; 
All = "*" | "All"   
``` 

### Examples
  
Given the rule `"exclude  Brand 'ONLY' when input product's artikeltype is 'Parka'"`, it can be specified with the RuleDSL as follows.  

```
rule "exclude  Brand 'ONLY' when artikeltype is 'Parka'":
    when
        $ctx.artikeltype is "Parka (Dames)"
    show
        product with merk not eq "Only"
```

Note that the a business rule has two blocks, `when` and `select`. 

- The predicate enclosed in the `when` describes the condition(s) under which this rule is applicable.
- Given the rule is applicable, `select` blocks describes what would be the resulting recommendations after the rule is applied.     

As a syntactic sugar, instead of `show`, one can use `select` to denote the selected recommendations. Given the semantics, anyone of the keyword can be used in this context.

Similarly, the following code specify rule `"exclude products that relate to 'intimiteit' from all non-'intimiteit'"`.

```
rule "exclude products that relate to 'intimiteit' from all non-'intimiteit'":
  when 
	  $ctx.productCategory is not “C29_9IJ” //input product is not `intimiteit` products
  select 
    Products
  with 
    category not eq “OPR_SMA_SMR” // exclude always `intimiteit`
```

### Semantics 

- In general, if a Recommendation System contains a RuleDSL program, by default it will **only**  return the recommendations resulted from the application of the rules on the initial result-set. 

- A RuleDSL program can consist of several business rules. If multiple rules become applicable due to the stated predicates in `when` , the end result will be intersections of the respective result-sets. That is, given that applicable rules are {R1,R2} and corresponding result-set is {P1, P2}, then the final result-set will be {P1 ∩ P2}.

### Development status

The current version (`v1.0.0`) constructs a SyntaxAnalyser for RuleDSL. The SyntaxAnalyser is implemented by using parser combinators. Hence, at present the compiler takes a RuleDSL file (`*.rbr`) as an input and transforms it to an Abstract Syntax Tree (AST). Given the following rule:

```
// Only select wehkamp products
// when input product is a wehkmap product
rule example0:
    when
        $ctx.brand eq "wehkamp"
    show
        product with category eq "C_WEHKAMP"
```

The compiler generates following AST as part of the compilation process. 

```scala 
RuleDslProgram (
    List (
        RuleDef (
            RuleId ("example0"),
            RuleBody (
                WhenExpr (
                    EqualsToExpr (
                        PropertyExpr (IdExpr ("ctx"), IdExpr ("brand")),
                        StringLiteral ("wehkamp"))),
                SelectionExpr (
                    DataSource ("products"),
                    WhereExpr (
                        EqualsToExpr (
                            IdExpr ("category"),
                            StringLiteral ("C_WEHKAMP"))))))))
```

Note that, in future version, we plan to include a `Semantic Analyser` and an Interpreter` for the compiler to transform the AST to filtered recommendations.   

## RuleDSL in Action

### Using RuleDslREPL

The DSL includes a naive REPL that allows to type a rule and see the output in the REPL. 

To do that, please invoke the following command in REPL.

```
$ sbt "run-main rfs.blaze.recommendations.ruledsl.RuleDslREPL"
```

It would intialise the REPL as follows:

```
[info] Running rfs.blaze.recommendations.ruledsl.RuleDslREPL
Enter a rule to parse.
  e.g., rule A : when true show all products

ruledsl>

```

Please enter a RuleDSL program to see the output from the REPL. 

```scala 
ruledsl> rule example3: when $ctx.advisor is "C21_1A9_A1L" show products with advisor eq $ctx.advisor
e tree:
RuleDslProgram (
    List (
        RuleDef (
            RuleId ("example3"),
            RuleBody (
                WhenExpr (
                    EqualsToExpr (
                        PropertyExpr (IdExpr ("ctx"), IdExpr ("advisor")),
                        StringLiteral ("C21_1A9_A1L"))),
                SelectionExpr (
                    DataSource ("products"),
                    WhereExpr (
                        EqualsToExpr (
                            IdExpr ("advisor"),
                            PropertyExpr (
                                IdExpr ("ctx"),
                                IdExpr ("advisor")))))))))
                    
``` 
 
### Using RuleDSL Compiler

As stated previously, the current version of the library only includes a parser for the BusinessRule DSL.

To experiment with the DSL, please follow the steps below:

- Start `SBT` in the terminal (at the root directory of this project). 

```
$ sbt 
```

- Execute the following command in SBT shell. 
 
 ```
 > run-main  rfs.blaze.recommendations.ruledsl.RuleDslCompiler filepath
 ```
 
Here the filepath refers to the file path of the RuleDsl program. To see examples, please navigate to the 
`resources` directory under `src/main`.

Consider src/main/resources/3.rbr, which contains following rule:

```scala
rule example3: 
    when $ctx.advisor is "C21_1A9_A1L" 
    show products 
    with advisor eq $ctx.advisor
```

By invoking following, we can see the AST resulted from the compiler. 

```scala
$ sbt
> run-main  rfs.blaze.recommendations.ruledsl.RuleDslCompiler src/main/resources/3.rbr

RuleDslProgram (
    List (
        RuleDef (
            RuleId ("example3"),
            RuleBody (
                WhenExpr (
                    EqualsToExpr (
                        PropertyExpr (IdExpr ("ctx"), IdExpr ("advisor")),
                        StringLiteral ("C21_1A9_A1L"))),
                SelectionExpr (
                    DataSource ("products"),
                    WhereExpr (
                        EqualsToExpr (
                            IdExpr ("advisor"),
                            PropertyExpr (
                                IdExpr ("ctx"),
                                IdExpr ("advisor")))))))))
Output written to the following file: src/main/resources/1.out
[success] Total time: 0 s, completed Nov 23, 2015 5:25:12 PM

```

 
# Using RuleDSL as a Library

To use this library, first we add following to the SBT dependencies in the project. 

```scala 

"com.rfs" %%  "blaze-lib-recommendation-rule-dsl" % "1.0.0"

```

We add following `import` in the file where we want to use the RuleDSL: 

```scala 

import rfs.blaze.recommendations.ruledsl._

```

and parse an input string to AST as follows.

```scala
RuleSyntaxAnalyser.parse(ruleDslProgram)

```

Note that invoking `parse` results in `ParseResult` which can be `Success`, `Failure` or `Error`. 

```
RuleSyntaxAnalyser.parse(program1) match {
    case Success(result, _)   ⇒ result
    case Failure(errMsg, _) ⇒ errMsg 
    case Error(errMsg, _) ⇒ errMsg  
}
```

See [scaladoc](http://www.scala-lang.org/api/2.11.6/scala-parser-combinators/index.html#scala.util.parsing.combinator.Parsers$ParseResult) for the detail semeantics of `ParseResult`.