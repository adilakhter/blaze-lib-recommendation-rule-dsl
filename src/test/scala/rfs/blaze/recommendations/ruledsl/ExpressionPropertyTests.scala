package rfs.blaze
package recommendations
package ruledsl

import org.scalacheck._
import org.scalacheck.Gen._
import org.scalacheck.Prop._

object ExpressionPropertyTests extends Properties("RuleParsers") with RuleParsers {

  val trueExprGen: Gen[String] = for {
    boolValue ← Gen.oneOf("true", "always")
  } yield boolValue

  val falseExprGen: Gen[String] = for {
    boolValue ← Gen.oneOf("false", "never")
  } yield boolValue

  val allExprGen: Gen[String] = for {
    boolValue ← Gen.oneOf("*", "all")
  } yield boolValue

  property("should be able to parse an arbitrary string literal") = forAll(Gen.alphaStr) { expr: String ⇒
    parseAll(string, "\"" + expr + "\"").get == StringLiteral(expr)
  }

  property("should be able to parse any natural number") = forAll(posNum[Int]) { expr: Int ⇒
    parseAll(number, expr.toString).get == NumberExpr(expr)
  }

  property("should be able to parse true expression") = forAll(trueExprGen) { expr: String ⇒
    parseAll(trueExpr, expr).get == TrueExpr
  }

  property("should be able to parse false expression") = forAll(falseExprGen) { expr: String ⇒
    parseAll(falseExpr, expr).get == FalseExpr
  }

  property("should be able to parse all expression") = forAll(allExprGen) { expr: String ⇒
    parseAll(all, expr).get == All
  }

  property("should be able to parse ID expression") = forAll(identifier) { expression: String ⇒
    parseAll(idExpr, expression).get == IdExpr(expression)
  }
}
