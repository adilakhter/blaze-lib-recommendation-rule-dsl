package rfs.blaze.recommendations.ruledsl

import org.kiama.util.RegexParserTests

class ExpressionsTests extends RuleParsers with RegexParserTests {

  test("should parse negative number should result in errors") {
    assertParseError("-1", number, 1, 1, """string matching regex `(\d+(\.\d*)?|\d*\.\d+)' expected but `-' found""")
  }

  test("should parse zero") {
    assertParseOk("0", number, NumberExpr(0))
  }

  test("should parse an integer") {
    assertParseOk("42", number, NumberExpr(42))
  }

  test("should parse a empty String") {
    val stringToParse = ""
    assertParseOk("\"" + stringToParse + "\"", string, StringLiteral(stringToParse))
  }

  test("should parse a String without spaces") {
    val stringToParse = "teststring"
    assertParseOk("\"" + stringToParse + "\"", string, StringLiteral(stringToParse))
  }

  test("should parse a String with spaces") {
    val stringToParse = "teststring"
    assertParseOk("\"" + stringToParse + "\"", string, StringLiteral(stringToParse))
  }

  test("should parse a propertyName") {
    val expressionString = "$ctx.intputProductName"
    val parsedExpression = PropertyExpr(IdExpr("ctx"), IdExpr("intputProductName"))
    assertParseOk(expressionString, propertyExpr, parsedExpression)
  }

  test("should parse `true` without errors to TrueExpr") {
    val expressionString = "true"
    val parsedExpression = TrueExpr
    assertParseOk(expressionString, trueExpr, parsedExpression)
  }

  test("should parse `always` expr without errors to TrueExpr") {
    val expressionString = "always"
    val parsedExpression = TrueExpr
    assertParseOk(expressionString, trueExpr, parsedExpression)
  }

  test("should parse `false` expr without errors to FalseExpr") {
    val expressionString = "false"
    val parsedExpression = FalseExpr
    assertParseOk(expressionString, falseExpr, parsedExpression)
  }

  test("should parse `never` expr without errors to FalseExpr") {
    val expressionString = "never"
    val parsedExpression = FalseExpr
    assertParseOk(expressionString, falseExpr, parsedExpression)
  }
}
