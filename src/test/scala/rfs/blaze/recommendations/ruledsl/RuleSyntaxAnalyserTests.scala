package rfs.blaze.recommendations.ruledsl

import org.scalatest.FlatSpec
import org.scalatest.prop.TableDrivenPropertyChecks

class RuleSyntaxAnalyserTests extends FlatSpec with TableDrivenPropertyChecks {
  "A RuleDSL program" should "be parsed by SyntaxAnalyser to the expected AST" in {
    forAll(ruleDslPrograms) {
      (input, expected) ⇒
        val result = RuleSyntaxAnalyser.parse(input).get
        assert(result === expected)
    }
  }

  val ruleDslPrograms = Table(
    ("input", "expected"),
    ("""rule example1: show all products""",
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example1"),
            RuleBody(
              WhenExpr(TrueExpr),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(TrueExpr))))))),
    ("""rule example01: select products with artikeltype eq "shoe" or category not eq "C29_9IJ"""",
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example01"),
            RuleBody(
              WhenExpr(TrueExpr),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(
                  OrExpr(
                    EqualsToExpr(
                      IdExpr("artikeltype"),
                      StringLiteral("shoe")),
                    NotEqualToExpr(
                      IdExpr("category"),
                      StringLiteral("C29_9IJ")))))))))),
    ("""rule example02: select products with artikeltype is not "shoe" and category is not "C29_9IJ"""",
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example02"),
            RuleBody(
              WhenExpr(TrueExpr),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(
                  AndExpr(
                    NotEqualToExpr(
                      IdExpr("artikeltype"),
                      StringLiteral("shoe")),
                    NotEqualToExpr(
                      IdExpr("category"),
                      StringLiteral("C29_9IJ")))))))))),
    ("""rule example03: select products with artikeltype is not "shoe" and category in ["C29_9IJ"]""",
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example03"),
            RuleBody(
              WhenExpr(TrueExpr),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(
                  AndExpr(
                    NotEqualToExpr(
                      IdExpr("artikeltype"),
                      StringLiteral("shoe")),
                    InExpr(
                      IdExpr("category"),
                      ValueList(
                        List(StringLiteral("C29_9IJ")))))))))))),
    ("""rule example04: select products with artikeltype is "shoe" and category is not "C29_9IJ" or category is not "CJ"""", RuleDslProgram(
      List(
        RuleDef(
          RuleId("example04"),
          RuleBody(
            WhenExpr(TrueExpr),
            SelectionExpr(
              DataSource("products"),
              WhereExpr(
                OrExpr(
                  AndExpr(
                    EqualsToExpr(
                      IdExpr("artikeltype"),
                      StringLiteral("shoe")),
                    NotEqualToExpr(
                      IdExpr("category"),
                      StringLiteral("C29_9IJ"))),
                  NotEqualToExpr(
                    IdExpr("category"),
                    StringLiteral("CJ")))))))))),
    ("""rule example11: when $ctx.brand eq "nike" and $ctx.brand not eq "addidas" select all products""",
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example11"),
            RuleBody(
              WhenExpr(
                AndExpr(
                  EqualsToExpr(
                    PropertyExpr(
                      IdExpr("ctx"),
                      IdExpr("brand")),
                    StringLiteral("nike")),
                  NotEqualToExpr(
                    PropertyExpr(
                      IdExpr("ctx"),
                      IdExpr("brand")),
                    StringLiteral("addidas")))),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(TrueExpr))))))),
    ("""rule example22: when $ctx.brand not eq "nike" or $ctx.brand in ["addidas"] select all products""",
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example22"),
            RuleBody(
              WhenExpr(
                OrExpr(
                  NotEqualToExpr(
                    PropertyExpr(
                      IdExpr("ctx"),
                      IdExpr("brand")),
                    StringLiteral("nike")),
                  InExpr(
                    PropertyExpr(
                      IdExpr("ctx"),
                      IdExpr("brand")),
                    ValueList(List(StringLiteral("addidas")))))),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(TrueExpr))))))),
    ("""rule example33: when $ctx.brand in ["addidas", "nike"] and $ctx.articleType is "gadgets" show  all products""", RuleDslProgram(
      List(
        RuleDef(
          RuleId("example33"),
          RuleBody(
            WhenExpr(
              AndExpr(
                InExpr(
                  PropertyExpr(
                    IdExpr("ctx"),
                    IdExpr("brand")),
                  ValueList(
                    List(
                      StringLiteral("addidas"),
                      StringLiteral("nike")))),
                EqualsToExpr(
                  PropertyExpr(
                    IdExpr("ctx"),
                    IdExpr("articleType")),
                  StringLiteral("gadgets")))),
            SelectionExpr(
              DataSource("products"),
              WhereExpr(TrueExpr))))))),
    ("""rule example44: when $ctx.brand in ["addidas", "nike"] and ($ctx.articleType is "gadgets" or $ctx.articleType is "cloths")  show all products """,
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example44"),
            RuleBody(
              WhenExpr(
                AndExpr(
                  InExpr(
                    PropertyExpr(
                      IdExpr("ctx"),
                      IdExpr("brand")),
                    ValueList(
                      List(
                        StringLiteral("addidas"),
                        StringLiteral("nike")))),
                  OrExpr(
                    EqualsToExpr(
                      PropertyExpr(
                        IdExpr("ctx"),
                        IdExpr("articleType")),
                      StringLiteral("gadgets")),
                    EqualsToExpr(
                      PropertyExpr(
                        IdExpr("ctx"),
                        IdExpr("articleType")),
                      StringLiteral("cloths"))))),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(TrueExpr))))))),
    ("""rule example5: when $ctx.brand eq "nike" and $ctx.brand not eq "addidas" select products with artikeltype eq "shoe"""",
      RuleDslProgram(
        List(
          RuleDef(
            RuleId("example5"),
            RuleBody(
              WhenExpr(
                AndExpr(
                  EqualsToExpr(
                    PropertyExpr(
                      IdExpr("ctx"),
                      IdExpr("brand")),
                    StringLiteral("nike")),
                  NotEqualToExpr(
                    PropertyExpr(
                      IdExpr("ctx"),
                      IdExpr("brand")),
                    StringLiteral("addidas")))),
              SelectionExpr(
                DataSource("products"),
                WhereExpr(
                  EqualsToExpr(
                    IdExpr("artikeltype"),
                    StringLiteral("shoe"))))))))),
    ("""rule example14: when false show all products""", RuleDslProgram(
      List(
        RuleDef(
          RuleId("example14"),
          RuleBody(
            WhenExpr(FalseExpr),
            SelectionExpr(
              DataSource("products"),
              WhereExpr(TrueExpr))))))),
    ("""rule example15: when never show all products""", RuleDslProgram(
      List(
        RuleDef(
          RuleId("example15"),
          RuleBody(
            WhenExpr(FalseExpr),
            SelectionExpr(
              DataSource("products"),
              WhereExpr(TrueExpr)))))))
  )
}
