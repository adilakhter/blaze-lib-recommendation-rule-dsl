package rfs.blaze.recommendations.ruledsl

import org.kiama.util._

class RuleParsersTests extends RuleParsers with RegexParserTests {

  test("should parse empty RuleDSL program") {
    val program = "".stripMargin

    assertParseOk(program, parser, RuleDslProgram(List()))
  }

  test("should parse a rule program with a simple rule definition") {
    val program = "rule example1: show all products"

    assertParseOk(program, parser, RuleDslProgram(
      List(
        RuleDef(
          RuleId("example1"),
          RuleBody(
            WhenExpr(TrueExpr),
            SelectionExpr(
              DataSource("products"),
              WhereExpr(TrueExpr)))))))

  }

  test("should parse multiple rules specified in a RuleDsl program") {
    val program =
      """
        |rule example1: show all products
        |rule example2: select all product
      """.stripMargin

    assertParseOk(program, parser, RuleDslProgram(
      List(
        RuleDef(
          RuleId("example1"),
          RuleBody(
            WhenExpr(TrueExpr),
            SelectionExpr(
              DataSource("products"),
              WhereExpr(TrueExpr)))),
        RuleDef(
          RuleId("example2"),
          RuleBody(
            WhenExpr(TrueExpr),
            SelectionExpr(
              DataSource("product"),
              WhereExpr(TrueExpr)))))))
  }

  test("should parse RuleDsl program with comments includes") {
    val program =
      """
        |// Selects all product
        |rule selectAll: select all product
      """.stripMargin

    assertParseOk(program, parser, RuleDslProgram(
      List(
        RuleDef(
          RuleId("selectAll"),
          RuleBody(
            WhenExpr(TrueExpr),
            SelectionExpr(
              DataSource("product"),
              WhereExpr(TrueExpr)))))))
  }

  test("should parse `select all` same as `select *`") {
    val program1 =
      """
        | rule selectAll: select all product
      """.stripMargin

    val program2 =
      """
        | rule selectAll: select * product
        |
      """.stripMargin

    val ast1 = RuleSyntaxAnalyser.parse(program1).get
    val ast2 = RuleSyntaxAnalyser.parse(program2).get

    assert(ast1 == ast2) // ast1 should be equals to ast2
  }

  test("should parse when a rule program both activator and selector specified") {
    val program =
      """
        | rule example:
        |    when
        |        $ctx.brand in ["nike"]
        |    select
        |        products with category in $ctx.categories
      """.stripMargin

    assertParseOk(program, parser, RuleDslProgram(
      List(
        RuleDef(
          RuleId("example"),
          RuleBody(
            WhenExpr(
              InExpr(
                PropertyExpr(IdExpr("ctx"), IdExpr("brand")),
                ValueList(List(StringLiteral("nike"))))),
            SelectionExpr(
              DataSource("products"),
              WhereExpr(
                InExpr(
                  IdExpr("category"),
                  PropertyExpr(
                    IdExpr("ctx"),
                    IdExpr("categories"))))))))))
  }

}