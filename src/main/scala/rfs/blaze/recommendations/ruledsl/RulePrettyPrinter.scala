package rfs.blaze
package recommendations
package ruledsl

import org.kiama.output._

/**
 * Rule AST pretty-printing.
 */
object RulePrettyPrinter extends PrettyPrinter