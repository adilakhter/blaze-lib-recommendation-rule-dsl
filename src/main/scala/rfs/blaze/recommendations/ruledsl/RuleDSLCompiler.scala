package rfs.blaze
package recommendations
package ruledsl

import org.kiama.util._

object RuleDSLCompiler extends Compiler[RuleDslProgram] with RuleParsers {

  /**
   * Process the source tree by analysing it to check for semantic
   * errors. If any messages are produced, print them. If all is ok,
   * translate the program and generate code for the translation.
   */
  override def process(filename: String, ast: RuleDslProgram, config: Config) {
    super.process(filename, ast, config)

    // Pretty print the abstract syntax tree
    config.output.emitln(RulePrettyPrinter.pretty_any(ast))
    val outputFileName = filename.replaceAll("\\.[^.]*$", "") + ".out"
    config.output.emitln("Output written to the following file: " + outputFileName)

    writeOutputToAFile(outputFileName, RulePrettyPrinter.pretty_any(ast))

  }
}