package rfs.blaze
package recommendations
package ruledsl

object RuleSyntaxAnalyser extends RuleParsers {

  def parseThis[T](p: PackratParser[T], s: String): ParseResult[T] = {
    this.parseAll(p, s)
  }

  def parse(in: java.lang.CharSequence): ParseResult[RuleDslProgram] = {
    this.parseAll(parser, in)
  }
}
