package rfs.blaze
package recommendations
package ruledsl

import org.kiama.util._

object RuleDSLREPL extends ParsingREPL[RuleDslProgram] with RuleParsers {

  import org.kiama.util.REPLConfig

  val banner =
    """Enter a rule to parse.
      |  e.g., rule A { when true then select * from products }
      |""".stripMargin

  override def prompt() = "ruledsl> "

  /**
   * Print the expression as a value, as a tree, pretty printed.
   * Print its value. Optimise it and then print the optimised
   * expression and its value.
   */
  override def process(e: RuleDslProgram, config: REPLConfig) {
    val output = config.output
    output.emitln("e tree:")
    output.emitln(RulePrettyPrinter.pretty_any(e))
  }
}
