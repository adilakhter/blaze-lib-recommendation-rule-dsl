package rfs.blaze
package recommendations

import java.io._

package object ruledsl {

  def using[A <: { def close(): Unit }, B](param: A)(f: A ⇒ B): B =
    try { f(param) } finally { param.close() }

  def writeOutputToAFile(fileName: String, data: String) =
    using(new FileWriter(fileName)) {
      fileWriter ⇒ fileWriter.write(data)
    }
}

