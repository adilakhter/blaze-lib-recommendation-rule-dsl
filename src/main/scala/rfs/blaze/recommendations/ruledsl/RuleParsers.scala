package rfs.blaze
package recommendations
package ruledsl

import org.kiama.util._

trait RuleParsers extends PositionedParserUtilities {

  lazy val parser = phrase(ruleProgram)

  lazy val ruleProgram: PackratParser[RuleDslProgram] = rep(ruleDef) ^^ RuleDslProgram

  lazy val ruleDef: PackratParser[RuleDef] = ("rule" ~> ruleName) ~ (":" ~> ruleBody) ^^ { case rName ~ rBody ⇒ RuleDef(rName, rBody) }

  lazy val ruleBody: PackratParser[RuleBody] = (opt(whenExpression) ~ includeExpr) ^^ {
    case None ~ selector           ⇒ RuleBody(WhenExpr(TrueExpr), selector)
    case Some(whenExpr) ~ selector ⇒ RuleBody(whenExpr, selector)
  }

  lazy val includeExpr = "select" ~> fromExpr | "show" ~> fromExpr

  lazy val fromExpr: PackratParser[SelectionExpr] =
    all ~> datasource ^^ { case ds ⇒ SelectionExpr(ds, WhereExpr(TrueExpr)) } |
      datasource ~ ("with" ~> opt(whereClause)) ^^ {
        case ds ~ None       ⇒ SelectionExpr(ds, WhereExpr(TrueExpr))
        case ds ~ Some(expr) ⇒ SelectionExpr(ds, expr)
      }

  lazy val whenExpression: PackratParser[WhenExpr] = "when" ~> predicate ^^ WhenExpr

  lazy val whereClause: PackratParser[WhereExpr] = predicate ^^ WhereExpr

  lazy val predicate: PackratParser[Expr] =
    predicate ~ ("and" ~> relExpression) ^^ AndExpr |
      predicate ~ ("or" ~> relExpression) ^^ OrExpr |
      relExpression

  lazy val relExpression: PackratParser[Expr] =
    relExpression ~ ("is" ~> literals) ^^ EqualsToExpr |
      relExpression ~ ("eq" ~> literals) ^^ EqualsToExpr |
      relExpression ~ ("equals" ~> "to" ~> literals) ^^ EqualsToExpr |
      relExpression ~ ("is" ~> "not" ~> literals) ^^ NotEqualToExpr |
      relExpression ~ ("not" ~> "eq" ~> literals) ^^ NotEqualToExpr |
      relExpression ~ ("not" ~> "equals" ~> "to" ~> literals) ^^ NotEqualToExpr |
      relExpression ~ ("in" ~> literals) ^^ InExpr |
      relExpression ~ ("not" ~> "in" ~> literals) ^^ NotInExpr |
      factor

  lazy val factor: PackratParser[Expr] = all | literals | "(" ~> predicate <~ ")" | idExpr

  lazy val idExpr: PackratParser[Expr] = not(keyword) ~> "[a-zA-Z][a-zA-Z0-9]*".r ^^ IdExpr

  lazy val propertyExpr: PackratParser[Expr] = "$" ~> idExpr ~ ("." ~> idExpr) ^^ PropertyExpr

  lazy val literals: PackratParser[Expr] = number | string | list | trueExpr | falseExpr | propertyExpr

  lazy val list: PackratParser[ValueList] = "[" ~> repsep(literals, ",") <~ "]" ^^ ValueList

  lazy val number: PackratParser[NumberExpr] = """(\d+(\.\d*)?|\d*\.\d+)""".r ^^ { case s ⇒ NumberExpr(s.toInt) }

  lazy val string: PackratParser[StringLiteral] = ("\"" + """([^"\p{Cntrl}\\]|\\[\\'"bfnrt]|\\u[a-fA-F0-9]{4})*+""" + "\"").r ^^ { case s ⇒ StringLiteral(s.substring(1, s.length - 1)) }

  lazy val trueExpr: PackratParser[BooleanExpr] = ("true" | "always") ^^^ TrueExpr

  lazy val falseExpr: PackratParser[BooleanExpr] = ("false" | "never") ^^^ FalseExpr

  lazy val datasource = ("products" | "product") ^^ DataSource

  lazy val all = ("*" | "all") ^^^ All

  lazy val ruleName = string ^^ { case StringLiteral(value) ⇒ RuleId(value) } | idExpr ^^ { case IdExpr(value) ⇒ RuleId(value) }

  lazy val keyword = keywords("[^a-zA-Z]".r, List("always", "never", "rule", "product", "products", "show", "select", "from", "where", "and", "or", "is", "in", "not", "all", "when", "criteria"))

  override val whiteSpace = """(\s|(//.*\n))+""".r
}
