package rfs.blaze
package recommendations
package ruledsl

import scala.util.parsing.input._
import org.kiama.attribution._

abstract class SourceNode extends Attributable with Positional
abstract class Expr extends SourceNode

case class RuleDslProgram(ruleDefs: Seq[RuleDef]) extends Expr
case class RuleDef(name: RuleId, ruleBody: RuleBody) extends Expr
case class RuleId(name: String)
case class RuleBody(when: WhenExpr, selection: SelectionExpr) extends Expr

case class SelectionExpr(ds: DataSource, whereExpr: Expr) extends Expr
case class WhereExpr(predicate: Expr) extends Expr

case class DataSource(name: String) extends Expr

sealed abstract class Value extends Expr
case class ValueList(values: List[Expr]) extends Value
case class NumberExpr(value: Int) extends Value
case class StringLiteral(value: String) extends Value

case class IdExpr(value: String) extends Expr
case class PropertyExpr(context: Expr, key: Expr) extends Expr
case object All extends Expr

sealed abstract class BooleanExpr extends Expr
case object TrueExpr extends BooleanExpr
case object FalseExpr extends BooleanExpr
case class AndExpr(left: Expr, right: Expr) extends BooleanExpr
case class OrExpr(left: Expr, right: Expr) extends BooleanExpr
case class NotExpr(exp: BooleanExpr) extends BooleanExpr

abstract class BinaryBooleanExpr(opName: String) extends BooleanExpr {
  def left: Expr
  def right: Expr
}
case class EqualsToExpr(left: Expr, right: Expr) extends BinaryBooleanExpr("IS")
case class NotEqualToExpr(left: Expr, right: Expr) extends BinaryBooleanExpr("IS NOT")
case class InExpr(left: Expr, right: Expr) extends BinaryBooleanExpr("IN")
case class NotInExpr(left: Expr, right: Expr) extends BinaryBooleanExpr("NOT IN")

case class WhenExpr(predicate: Expr) extends Expr

