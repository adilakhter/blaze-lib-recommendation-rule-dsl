import sbt._
import sbt.Keys._

import blaze.sbt._
import Dependencies._

object Build extends BlazeBuild {

  lazy val root = Project("blaze-lib-recommendation-rule-dsl", file("."))
    .enablePlugins(BlazeLibPlugin)
    .settings(version := "1.0.1-SNAPSHOT")
    .settings(libraryDependencies ++=
      compile(
        kiama,
        blazeLibCommon,
        scalaLogging
      ) ++
      test(
        blazeLibTestkit,
        scalatest,
        kiamaTest,
        scalaCheck
      )
    )
}
 