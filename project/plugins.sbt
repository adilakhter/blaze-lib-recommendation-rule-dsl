resolvers += Resolver.url("blaze-plugin-releases", url("https://dl.bintray.com/blaze-plugins/releases"))(Resolver.ivyStylePatterns)

addSbtPlugin("rfs.blaze" %% "sbt-blaze" % "0.14.10")
