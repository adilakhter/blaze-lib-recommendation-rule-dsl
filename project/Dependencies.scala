import sbt._
import sbt.Keys._

object Dependencies {
  // compile-time dependencies
  val scalaLogging             = "com.typesafe.scala-logging"     %% "scala-logging"                 % "3.1.0"
  val blazeLibCommon           = "com.rfs"                        %% "blaze-lib-common"              % "1.2.0"
  val scalaParserCombinators   = "org.scala-lang.modules"         %% "scala-parser-combinators"      % "1.0.4"
  val kiama                    = "com.googlecode.kiama"           %% "kiama"                         % "1.8.0"
  
  // test dependencies
  val scalatest                = "org.scalatest"                 %% "scalatest"                     % "2.2.5"
  val scalaCheck               = "org.scalacheck"                %% "scalacheck"                    % "1.12.5"
  val blazeLibTestkit          = "com.rfs"                       %% "blaze-lib-testkit"             % "3.0.0"
  val kiamaTest                = "com.googlecode.kiama"          %% "kiama"                         % "1.8.0" classifier ("tests")
}